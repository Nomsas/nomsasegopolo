import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ccna4',
  templateUrl: './ccna4.component.html',
  styleUrls: ['./ccna4.component.scss']
})
export class Ccna4Component implements OnInit {

  constructor() { }

  imageSrc = '/assets/CCNA 4.jpg'
  imageAlt = 'Cover Photo'

  ngOnInit(): void {
  }

}
