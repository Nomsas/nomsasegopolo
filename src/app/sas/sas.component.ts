import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sas',
  templateUrl: './sas.component.html',
  styleUrls: ['./sas.component.scss']
})
export class SasComponent implements OnInit {

  constructor() { }

  imageSrc = '/assets/SAS.jpg'
  imageAlt = 'Cover Photo'

  ngOnInit(): void {
  }

}
