import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-matric',
  templateUrl: './matric.component.html',
  styleUrls: ['./matric.component.scss']
})
export class MatricComponent implements OnInit {

  constructor() { }
  imageSrc = '/assets/Matric.jpg'
  imageAlt = 'Cover Photo'

  ngOnInit(): void {
  }

}
