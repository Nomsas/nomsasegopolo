import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ccna3',
  templateUrl: './ccna3.component.html',
  styleUrls: ['./ccna3.component.scss']
})
export class Ccna3Component implements OnInit {

  constructor() { }

  imageSrc = '/assets/CCNA2.jpg'
  imageAlt = 'Cover Photo'

  ngOnInit(): void {
  }

}
