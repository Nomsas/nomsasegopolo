import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  imageSrc = '/assets/Lame.jpg'
  imageAlt = 'Cover Photo'
  
  constructor() { }

  ngOnInit(): void {
  }

}
