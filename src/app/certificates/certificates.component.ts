import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.scss']
})
export class CertificatesComponent implements OnInit {
  
  constructor(private router:Router) { }
  
  goToPage(pageName:string):void{
  this.router.navigate(['S{pageName}'])
}

  ngOnInit(): void {
  }

}
