import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import { DefaultComponent } from './admin/default/default.component';
import { SidebarComponent } from './admin/shared/sidebar/sidebar.component';
import { MatricComponent } from './matric/matric.component';
import { DiplomaComponent } from './diploma/diploma.component';
import { IbmBadgesComponent } from './ibm-badges/ibm-badges.component';
import { SasComponent } from './sas/sas.component';
import { Ccna1Component } from './ccna1/ccna1.component';
import { Ccna2Component } from './ccna2/ccna2.component';
import { Ccna3Component } from './ccna3/ccna3.component';
import { Ccna4Component } from './ccna4/ccna4.component';
import { ExperienceComponent } from './experience/experience.component';
import { CertificatesComponent } from './certificates/certificates.component';
import { EducationComponent } from './education/education.component';
import { HomeComponent } from './home/home.component';


const Ux_Modules = [
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatToolbarModule,
  MatMenuModule
]

@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,
    SidebarComponent,
    MatricComponent,
    DiplomaComponent,
    IbmBadgesComponent,
    SasComponent,
    Ccna1Component,
    Ccna2Component,
    Ccna3Component,
    Ccna4Component,
    ExperienceComponent,
    CertificatesComponent,
    EducationComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    Ux_Modules
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
