import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ccna1',
  templateUrl: './ccna1.component.html',
  styleUrls: ['./ccna1.component.scss']
})
export class Ccna1Component implements OnInit {

  constructor() { }

  imageSrc = '/assets/CCNA 1.jpg'
  imageAlt = 'Cover Photo'

  ngOnInit(): void {
  }

}
