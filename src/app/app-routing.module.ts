import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './admin/default/default.component';
import { Ccna1Component } from './ccna1/ccna1.component';
import { Ccna2Component } from './ccna2/ccna2.component';
import { Ccna3Component } from './ccna3/ccna3.component';
import { Ccna4Component } from './ccna4/ccna4.component';
import { CertificatesComponent } from './certificates/certificates.component';
import { DiplomaComponent } from './diploma/diploma.component';
import { EducationComponent } from './education/education.component';
import { ExperienceComponent } from './experience/experience.component';
import { HomeComponent } from './home/home.component';
import { IbmBadgesComponent } from './ibm-badges/ibm-badges.component';
import { MatricComponent } from './matric/matric.component';
import { SasComponent } from './sas/sas.component';

const routes: Routes = [
  {path: '', component: DefaultComponent,
  children : [
    {path: 'home', component:HomeComponent},
    {path: 'education', component:EducationComponent },
    {path: 'certificates', component: CertificatesComponent},
    {path: 'experience', component:ExperienceComponent},
    {path: 'ccna1', component:Ccna1Component},
    {path: 'ccna2', component:Ccna2Component},
    {path: 'ccna3', component:Ccna3Component},
    {path: 'ccna4', component:Ccna4Component},
    {path: 'diploma', component:DiplomaComponent},
    {path: 'matric', component:MatricComponent},
    {path: 'sas', component:SasComponent},
    {path: 'ibm-badges', component:IbmBadgesComponent},
   
  ]
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
