import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ccna2',
  templateUrl: './ccna2.component.html',
  styleUrls: ['./ccna2.component.scss']
})
export class Ccna2Component implements OnInit {

  constructor() { }

  imageSrc = '/assets/CCNA 2.jpg'
  imageAlt = 'Cover Photo'

  ngOnInit(): void {
  }

}
